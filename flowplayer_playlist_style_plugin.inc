<?php
/**
 * @file
 * Provide a flowplayer playlist style plugin for Views.
 */

class flowplayer_playlist_style_plugin extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['default_dimensions'] = array('default' => '');
    $options['video_thumb_dimensions'] = array('default' => '');

    return $options;
  }

  /**
   * Provide form for style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['video_thumb_dimensions'] = array(
      '#title' => t('Video thumb dimensions'),
      '#type' => 'select',
      '#default_value' => $this->options['video_thumb_dimensions'],
      '#empty_option' => t('None (original video thumbnail)'),
      '#options' => image_style_options(FALSE),
    );

    $form['default_dimensions'] = array(
      '#title' => t('Output video dimensions'),
      '#type' => 'select',
      '#default_value' => $this->options['default_dimensions'],
      '#options' => video_utility::getDimensions(),
    );
  }

  /**
   * Provide render options.
   */
  function render() {
    if (!parent::uses_fields()){
      drupal_set_message('flowplayer requires Fields as row style', 'error');
      return;
    }

    flowplayer_add();
    drupal_add_js(drupal_get_path('module', 'flowplayer_playlist') .'/js/flowplayer.playlist-3.2.10.js');
    drupal_add_js(drupal_get_path('module', 'flowplayer_playlist') .'/js/jquery.carouFredSel-6.1.0.js');
    drupal_add_js(drupal_get_path('module', 'flowplayer_playlist') .'/js/flowplayer_playlist.js');
    drupal_add_css(drupal_get_path('module','flowplayer_playlist') . '/css/flowplayer_playlist.css');

    list($width, $height) = explode("x", $this->options['default_dimensions']);

    $sets = $this->render_grouping(
      $this->view->result,
      $this->options['grouping'],
      TRUE
    );

    $image_video = $sets['']['rows'][0]->field_field_video;
    $url_first_video = $image_video['0']['rendered']['#item']['thumbnailfile']->uri;
    $title_first_video = $image_video['0']['rendered']['#entity']->title;

    $image_video = theme_image_style(array(
        'style_name' => $this->options['video_thumb_dimensions'],
        'path' => $url_first_video,
        'alt' => $title_first_video,
        'title' => $title_first_video,
        "height" => NULL,
        "width" => NULL,
        'attributes' => array('class' => 'playlist-image'),
    ));
    
    // Render as a record set.
    foreach ($sets as $set) {
        foreach ($set['rows'] as $index => $row) {
            $this->view->row_index = $index;
            $set['rows'][$index] = $this->row_plugin->render($row);
        }
    }

    $output = theme('views_view_flowplayer_playlist',
      array(
      'view' => $this->view,
      'options' => $this->options,
      'title' => $set['group'],
      'image_video' => $image_video,
      'title_first_video' => $title_first_video,
      'rows' => $set['rows'],
      'width' => $width,
      'height' => $height,
    ));

    return $output;
  }

}

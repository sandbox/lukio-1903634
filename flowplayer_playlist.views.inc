<?php
/**
 * @file
 * Provide svg style plugin for Views.
 */

/**
  * Implements hook_views_plugin().
  */
function flowplayer_playlist_views_plugins() {
  return array(
    'style' => array(
      'flowplayer_playlist' => array(
        'title' => t('Flowplayer playlist'),
        'theme' => 'views_view_flowplayer_playlist',
        'help' => t('Display Flowplayer playlist style.'),
        'handler' => 'flowplayer_playlist_style_plugin',
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

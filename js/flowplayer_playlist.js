(function ($) {
  Drupal.behaviors.flowplayerPlaylist = {
    attach: function (context, settings) {

      try {
        flowplayer("play-button", "http://releases.flowplayer.org/swf/flowplayer-3.2.15.swf", {
          log: { level: 'error'},

          clip: {
            baseUrl:'',
            scaling:'scale'
          },
          }).playlist('ul.playlist-rows', {loop:false});
      } catch(err) {
        // Don't need to do anything on error.
      }

      $('ul.playlist-rows').once(function() {
        $(this).carouFredSel({
          circular: true,
          infinite: false,
          auto: false,
          scroll: 1,
          items: {
            minimum: 2,
            visible: 3
          },
          prev: {
            button: "#playlist-prev",
            key: "left"
          },
          next: {
            button: "#playlist-next",
            key: "right"
          }
        })
      });
      $(".playlist-rows a").click(function(event){
        var title = $(this).find('img')[0].attributes['title'].value;
        $('.placeholder-title-video').html(title);
      });
    }
  };
})(jQuery);



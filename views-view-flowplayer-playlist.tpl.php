<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="item-list">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <div class="make-playlist">
      <div class="play-button" id="play-button" style="width:<?php print $width; ?>px; height:<?php print $height; ?>px;"><?php print $image_video; ?></div>
      <div class="placeholder-title-video separador-top-10 white bold"><?php print $title_first_video; ?></div>
      <div class="image_carousel">
          <ul class="playlist-rows">
            <?php foreach ($rows as $id => $row): ?>
              <li class="playlist-thumbnail <?php print $classes; ?>"><?php print $row; ?></li>
            <?php endforeach; ?>
          </ul>
          <div class="clearfix"></div>
          <a class="prev" id="playlist-prev" href="#"><span>prev</span></a>
          <a class="next" id="playlist-next" href="#"><span>next</span></a>
      </div>
  </div>
</div>
